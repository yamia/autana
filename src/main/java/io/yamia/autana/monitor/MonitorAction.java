package io.yamia.autana.monitor;

public enum MonitorAction {
	START,
	EXECUTING,
	SUCCESS,
	FAIL,
	END
}